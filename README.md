# polystart

A program designed to easily start other programs according to user configurations and system defaults. Similar to `i3-sensible-*`, but more extensible.